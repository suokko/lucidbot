import api.events.bot.NonCommandEvent
import api.irc.communication.IRCAccess
import api.runtime.ServiceLocator
import database.daos.ProvinceDAO

def Class<NonCommandEvent> handles() {
    return NonCommandEvent.class;
}

def handleEvent(rawEvent) {
    def smartAssGuy = "LucidEssence"; //read this from file or whatever
    if (rawEvent instanceof NonCommandEvent) {
        def event = (NonCommandEvent) rawEvent;
        if (event.context.channel != null && event.context.user.mainNick.equals(smartAssGuy) && event.context.input == 'You suck') {
            def provinceDAO = ServiceLocator.lookup(ProvinceDAO.class);
            def ircAccess = ServiceLocator.lookup(IRCAccess.class);
            ircAccess.sendMessage(event.context.channel, "Hey smartass! Your province is: " + provinceDAO.getProvinceForUser(event.context.botUser)?.getName());
        }
    }
}