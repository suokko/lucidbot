/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.calculator.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.models.Province;
import database.models.SoT;
import database.models.SoM;
import database.models.SoS;
import database.models.Survey;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.BestMatchFinder;
import tools.calculators.WPACalculator;
import tools.calculators.MaxPopulationCalculator;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import lombok.extern.log4j.Log4j;

@Log4j
public class DesertCommandHandler implements CommandHandler {
    private final BestMatchFinder bestMatchFinder;
    private final WPACalculator wpaCalculator;
    private final MaxPopulationCalculator maxPopulationCalculator;

    @Inject
    public DesertCommandHandler(final BestMatchFinder bestMatchFinder, final WPACalculator wpaCalculator,
                                final MaxPopulationCalculator maxPopulationCalculator) {
        this.bestMatchFinder = bestMatchFinder;
        this.wpaCalculator = wpaCalculator;
        this.maxPopulationCalculator = maxPopulationCalculator;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Province province = bestMatchFinder.findBestMatch(params.isEmpty() ? context.getBotUser().getMainNick() : params.getParameter("nickOrProvince").trim());

            if (province == null) return CommandResponse.errorResponse("Found no matching user/province");
            if (province.getSot() == null) return CommandResponse.errorResponse("No SoT to calculate with for " + province.getName());

            Map<String, Integer> overPopulation = calcOverPopulation(province);

            if (overPopulation == null)
                return CommandResponse.errorResponse(province.getName() + " is not overpopulated enough to desert");

            return CommandResponse.resultResponse("result", overPopulation, "lastUpdated", province.getSot().getLastUpdated(),
                    "province", province.getName());

        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
    private Map<String, Integer> calcOverPopulation(final Province province) {
        Date intelExpireDate = new Date(System.currentTimeMillis() - 43200000);

        SoT sot = province.getSot();
        SoM som = province.getSom();
        Survey survey = province.getSurvey();
        SoS sos = province.getSos();

        if (som != null && province.getSom().getLastUpdated().before(intelExpireDate)) {som = null; }
        if (survey != null && province.getSurvey().getLastUpdated().before(intelExpireDate)) { survey = null; }
        if (sos != null && province.getSos().getLastUpdated().before(intelExpireDate)) { sos = null; }

        int elitesHome = sot.getElites();
        int offSpecsHome = sot.getOffSpecs();
        int soldiersHome = sot.getSoldiers();
        int defSpecsHome = sot.getDefSpecs();
        if (som != null) {
            elitesHome = som.getArmyHome().getElites();
            offSpecsHome = som.getArmyHome().getOffSpecs();
            defSpecsHome = som.getArmyHome().getDefSpecs();
            soldiersHome = som.getArmyHome().getSoldiers();
        }

        int pop = 0;
        pop += sot.getSoldiers();
        pop += sot.getPeasants();
        pop += sot.getDefSpecs();
        pop += sot.getOffSpecs();
        pop += sot.getElites();
        pop += province.getThieves();
        pop += province.getWizards() == 0 ? wpaCalculator.calcWizards(province, sot, sos, survey) : province.getWizards();

        int maxPop = maxPopulationCalculator.calcMaxPopulation(province, sot, sos, survey);

        double overPopulation = pop / maxPop;
        if (overPopulation < 1.15) return null;

        Map<String, Integer> overPop = new HashMap<String, Integer>();
        double desertionMetric = .2 * ((pop - sot.getPeasants() * .07) / (maxPop * 1.15) - 1);
        double elitesDeserted = (elitesHome * desertionMetric) > soldiersHome ? elitesHome * desertionMetric : 0;
        double thievesDeserted = (province.getThieves() * desertionMetric) > (soldiersHome - elitesDeserted) ? province.getThieves() * desertionMetric : 0;
        double defSpecsDeserted = (defSpecsHome * desertionMetric) > (soldiersHome - elitesDeserted - thievesDeserted) ? defSpecsHome * desertionMetric : 0;
        double offSpecsDeserted = (offSpecsHome * desertionMetric) > (soldiersHome - elitesDeserted - thievesDeserted - defSpecsDeserted) ? offSpecsHome * desertionMetric : 0;
        double soldiersDeserted = Math.max((soldiersHome - (elitesDeserted + thievesDeserted + defSpecsDeserted + offSpecsDeserted)) * desertionMetric, 0);

        overPop.put("elites", (int) elitesDeserted);
        overPop.put("thieves", (int) thievesDeserted);
        overPop.put("defspecs", (int) defSpecsDeserted);
        overPop.put("offspecs", (int) offSpecsDeserted);
        overPop.put("soldiers", (int) soldiersDeserted);
        overPop.put("cover", (int) (elitesHome * desertionMetric + province.getThieves() * desertionMetric +
                defSpecsHome * desertionMetric + offSpecsHome * desertionMetric));

        return overPop;
    }
}
