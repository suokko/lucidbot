package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV17ToV18 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 18;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Faery WPA', 'WPA', 'BOTH', true, 0.3)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Faery TPA', 'TPA', 'BOTH', true, 0.3)"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery WPA'"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery TPA'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Halfling' AND bonus.type = 'TPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Elf' AND bonus.type = 'WPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Elf WPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_def_strength = 4, elite_networth = 5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, elite_def_strength = 2, elite_networth = 5 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 4 WHERE name = 'Undead'")
        );
    }
}
