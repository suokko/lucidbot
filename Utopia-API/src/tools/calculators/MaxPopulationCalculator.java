package tools.calculators;

import api.tools.numbers.NumberUtil;
import database.models.*;
import tools.GameMechanicCalculator;
import tools.time.UtopiaTimeFactory;

import java.util.Date;
import javax.inject.Inject;
import java.util.Date;

/**
 * Created by Kyle on 10/6/2015.
 */
public class MaxPopulationCalculator {
    private final UtopiaTimeFactory utopiaTimeFactory;
    private final GameMechanicCalculator gameMechanicCalculator;

    @Inject
    public MaxPopulationCalculator(final UtopiaTimeFactory utopiaTimeFactory,
                                   final GameMechanicCalculator gameMechanicCalculator) {
        this.utopiaTimeFactory = utopiaTimeFactory;
        this.gameMechanicCalculator = gameMechanicCalculator;
    }

    public int calcMaxPopulation(final Province province, final SoT sot, final SoS sos, final Survey survey) {

        int currentLand = getCurrentLand(province, survey, sot);
        double housingScience = getHousingScience(sos);

        double basePop = survey == null ? province.getLand() * 25 :
                ((survey.getTotalBuilt() + survey.getTotalInProgress() - survey.getBuildingAmount("homes") - survey.getBuildingAmount("barren")) * 25 +
                        survey.getBuildingAmount("homes") * 35 + survey.getBuildingAmount("barren") * 15) * currentLand / (survey.getTotalBuilt() + survey.getTotalInProgress());

        return (int) (basePop * (1 + housingScience / 100));

    }

    private int getCurrentLand(final Province province, final Survey survey, final SoT sot) {

        if (survey == null || sot.getLastUpdated().after(survey.getLastUpdated())) {
            return province.getLand();
        } else {
            return survey.getTotalBuilt() + survey.getTotalInProgress();
        }
    }
    private double getHousingScience(final SoS sos) {

        if (sos == null) {
            int utoYear = utopiaTimeFactory.newUtopiaTime(System.currentTimeMillis()).getYear();
            int estimatedHousingBooks = utoYear * 60 / 4;
            String estimatedPercent = gameMechanicCalculator.getPercentFromBpa("housing", estimatedHousingBooks, null);

            return NumberUtil.parseDouble(estimatedPercent.replace("%", ""));

        } else {
            return sos.getScienceEffectByType("housing");
        }
    }

    public void updateMaxPopulation(final Province province) {
        if (province.getSot() != null) {
            int maxPop = calcMaxPopulation(province, province.getSot(), province.getSos(), province.getSurvey());
            setMaxPopulation(province, maxPop);
        }
    }

    public void updateMaxPopulation(final Province province, final SoT sot) {
        int maxPop = calcMaxPopulation(province, sot, province.getSos(), province.getSurvey());
        setMaxPopulation(province, maxPop);
    }

    public void updateMaxPopulation(final Province province, final SoS sos) {
        if (province.getSot() != null) {
            int maxPop = calcMaxPopulation(province, province.getSot(), sos, province.getSurvey());
            setMaxPopulation(province, maxPop);
        }
    }

    public void updateMaxPopulation(final Province province, final Survey survey) {
        if (province.getSot() != null) {
            int maxPop = calcMaxPopulation(province, province.getSot(), province.getSos(), survey);
            setMaxPopulation(province, maxPop);
        }
    }

    public void setMaxPopulation(final Province province, final int maxPop) {
        province.setMaxPopulation(maxPop);
    }
}
