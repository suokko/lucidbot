package events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SelfIntelPostedEvent {
    private final String intelType;
    private final long userId;
}
