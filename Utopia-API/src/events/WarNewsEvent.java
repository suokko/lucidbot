package events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Date;

import javax.annotation.Nullable;

@AllArgsConstructor
@Getter
public class WarNewsEvent {
    private final Date date;
    private final String newsType;
}
