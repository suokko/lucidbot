/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package tools.calculators;

import database.models.*;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import java.util.Date;

import lombok.extern.log4j.Log4j;

/**
 * A class that calculates a provinces WPA based off of SoT/Survey/SoS as available.  If Survey/SoS are not available it will estimate.
 */
@Log4j
public class WPACalculator {
    private final UtopiaTimeFactory utopiaTimeFactory;

    @Inject
    public WPACalculator(final UtopiaTimeFactory utopiaTimeFactory) {
        this.utopiaTimeFactory = utopiaTimeFactory;
    }

    public int calcWizards(final Province province, final SoT sot, final SoS sos, final Survey survey) {
        Race race = province.getRace();

        int nw = province.getNetworth();
        nw -= province.getThieves() * 4;
        nw -= sot.getSoldiers() * race.getSoldierNetworth();
        nw -= sot.getPeasants();
        nw -= sot.getDefSpecs() * race.getDefSpecStrength();
        nw -= sot.getOffSpecs() * race.getOffSpecStrength() * 0.8;
        nw -= sot.getElites() * race.getEliteNetworth();
        nw -= sot.getWarHorses() * 0.6;
        nw -= sot.getMoney() / 1000.0;

        int utoYear = (utopiaTimeFactory.newUtopiaTime(System.currentTimeMillis()).getYear());

        nw -= sos == null ? province.getLand() * utoYear * 60 / 92.0 : sos.getTotalBooks() / 92.0;
        nw -= survey == null ? province.getLand() * 55 : (survey.getTotalBuilt() - survey.getBuildingAmount("barren")) * 55
                + survey.getTotalInProgress() * 30 + survey.getBuildingAmount("barren") * 15;

        return Math.max(0, nw / 4);
    }
    public void updateWizards(final Province province) {
        if (!province.getWizardsAccurate() && province.getSot() != null) {
            int wizards = calcWizards(province, province.getSot(), province.getSos(), province.getSurvey());
            setWizards(province, wizards);
        }
    }

    public void updateWizards(final Province province, final SoT sot) {
        if (!province.getWizardsAccurate()) {
            int wizards = calcWizards(province, sot, province.getSos(), province.getSurvey());
            setWizards(province, wizards);
        }
    }

    public void updateWizards(final Province province, final SoS sos) {
        if (!province.getWizardsAccurate() && province.getSot() != null) {
            int wizards = calcWizards(province, province.getSot(), sos, province.getSurvey());
            setWizards(province, wizards);
        }
    }

    public void updateWizards(final Province province, final Survey survey) {
        if (!province.getWizardsAccurate() && province.getSot() != null) {
            int wizards = calcWizards(province, province.getSot(), province.getSos(), survey);
            setWizards(province, wizards);
        }
    }

    public void setWizards(final Province province, final int wizards) {
        province.setWizards(wizards);
        province.setWizardsLastUpdated(new Date());
        province.setWizardsAccurate(false);
    }
}