/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package database.updates;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import api.database.updates.DatabaseUpdater;
//This is Importing all of the h2 and mysql database update classes
import database.updates.h2.*;
import database.updates.mysql.*;


public class UtopiaDatabaseUpdatesModule extends AbstractModule {

  @Override
  protected void configure() {
    Multibinder<DatabaseUpdater> binder = Multibinder.newSetBinder(binder(), DatabaseUpdater.class);
    binder.addBinding().to(H2UpdateV1ToV2.class);
    binder.addBinding().to(MySQLUpdateV1ToV2.class);
    binder.addBinding().to(H2UpdateV3ToV4.class);
    binder.addBinding().to(MySQLUpdateV3ToV4.class);
    binder.addBinding().to(H2UpdateV4ToV5.class);
    binder.addBinding().to(MySQLUpdateV4ToV5.class);
    binder.addBinding().to(H2UpdateV5ToV6.class);
    binder.addBinding().to(MySQLUpdateV5ToV6.class);
    binder.addBinding().to(H2UpdateV6ToV7.class);
    binder.addBinding().to(MySQLUpdateV6ToV7.class);
    binder.addBinding().to(H2UpdateV7ToV8.class);
    binder.addBinding().to(MySQLUpdateV7ToV8.class);
    binder.addBinding().to(H2UpdateV8ToV9.class);
    binder.addBinding().to(MySQLUpdateV8ToV9.class);
    binder.addBinding().to(H2UpdateV9ToV10.class);
    binder.addBinding().to(MySQLUpdateV9ToV10.class);
    binder.addBinding().to(H2UpdateV10ToV11.class);
    binder.addBinding().to(MySQLUpdateV10ToV11.class);
    binder.addBinding().to(H2UpdateV11ToV12.class);
    binder.addBinding().to(MySQLUpdateV11ToV12.class);
    binder.addBinding().to(H2UpdateV12ToV13.class);
    binder.addBinding().to(MySQLUpdateV12ToV13.class);
    binder.addBinding().to(H2UpdateV13ToV14.class);
    binder.addBinding().to(MySQLUpdateV13ToV14.class);
    binder.addBinding().to(H2UpdateV14ToV15.class);
    binder.addBinding().to(MySQLUpdateV14ToV15.class);
    binder.addBinding().to(H2UpdateV15ToV16.class);
    binder.addBinding().to(MySQLUpdateV15ToV16.class);
    binder.addBinding().to(H2UpdateV16ToV17.class);
    binder.addBinding().to(MySQLUpdateV16ToV17.class);
    binder.addBinding().to(H2UpdateV17ToV18.class);
    binder.addBinding().to(MySQLUpdateV17ToV18.class);
    binder.addBinding().to(H2UpdateV18ToV19.class);
    binder.addBinding().to(MySQLUpdateV18ToV19.class);
    binder.addBinding().to(H2UpdateV19ToV20.class);
    binder.addBinding().to(MySQLUpdateV19ToV20.class);
    binder.addBinding().to(H2UpdateV20ToV21.class);
    binder.addBinding().to(MySQLUpdateV20ToV21.class);
    binder.addBinding().to(H2UpdateV21ToV22.class);
    binder.addBinding().to(MySQLUpdateV21ToV22.class);
    binder.addBinding().to(H2UpdateV22ToV23.class);
    binder.addBinding().to(MySQLUpdateV22ToV23.class);
    binder.addBinding().to(H2UpdateV23ToV24.class);
    binder.addBinding().to(MySQLUpdateV23ToV24.class);
    binder.addBinding().to(H2UpdateV24ToV25.class);
    binder.addBinding().to(MySQLUpdateV24ToV25.class);
    binder.addBinding().to(H2UpdateV25ToV26.class);
    binder.addBinding().to(MySQLUpdateV25ToV26.class);
  }
}
