package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV16ToV17 extends ApiMySQLDatabaseUpdater {

  @Override
  public int updatesToVersion() {
    return 17;
  }

  @Override
  public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
    return Lists.newArrayList(
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 where name = 'Orc Gains'"),
        new SimpleUpdateAction("UPDATE race SET elite_off_strength = 3, elite_networth = 5.25 WHERE name = 'Faery'"),
        new SimpleUpdateAction(
            "UPDATE race SET elite_off_strength = 4, soldier_strength = 2, elite_networth = 5.5 WHERE name = 'Halfling'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 3 WHERE name = 'Dwarf'")
    );
  }
}
