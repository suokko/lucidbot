package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV20ToV21 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 21;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN thieves_accurate BIT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN wizards_accurate BIT NOT NULL")
        );
    }
}