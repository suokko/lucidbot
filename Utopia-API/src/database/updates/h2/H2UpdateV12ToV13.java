package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV12ToV13 extends ApiH2DatabaseUpdater {

  @Override
  public int updatesToVersion() {
    return 13;
  }

  @Override
  public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
    return Lists.newArrayList(
        new SimpleUpdateAction(
            "DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE "
            +
            "race.name = 'Dwarf' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
        new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Dwarf Gains'"),
        new SimpleUpdateAction(
            "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES(" +
            "'Avian Gains', 'GAIN', 'OFFENSIVELY', false, 0.1)"),
        new SimpleUpdateAction(
            "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Avian' AND bonus.name = 'Avian Gain'"),
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 WHERE name = 'Orc Gains'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 3, elite_networth = 5.5 WHERE name = 'Dwarf'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 4, elite_networth = 5.75 WHERE name = 'Elf'"),
        new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_networth = 5.5 WHERE name = 'Faery'"),
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.15 WHERE name = 'Dragon Gains'"),
        new SimpleUpdateAction("UPDATE building_formula SET formula = '10*#amount#' WHERE formula = '8*#amount#'"),
        new SimpleUpdateAction(
            "UPDATE building_formula SET formula = '1*#amount#*#be#/100' WHERE formula = '2*#amount#*#be#/100'"),
        new SimpleUpdateAction(
            "UPDATE building_formula SET formula = '80*#amount#' WHERE formula = '20*#amount#' and result_text = 'Houses ? horses'")
    );
  }
}
