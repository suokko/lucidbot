package commands.management.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.OrderDAO;
import database.models.Order;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

public class RemoveAllOrdersCommandHandler implements CommandHandler {
    private final OrderDAO orderDAO;

    @Inject
    public RemoveAllOrdersCommandHandler(final OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            List<Order> orders;
            orders = orderDAO.getAllOrders();
            for (Order order : orders) {
                orderDAO.delete(order);
            }
            return CommandResponse.resultResponse("All orders removed", orders);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}