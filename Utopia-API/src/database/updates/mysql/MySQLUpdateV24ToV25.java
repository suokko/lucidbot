package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV24ToV25 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 25;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Lord Pop Bonus', 'POP', 'BOTH', true, .01) ON DUPLICATE KEY UPDATE bonus_value = .01"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Baron Pop Bonus', 'POP', 'BOTH', true, .02) ON DUPLICATE KEY UPDATE bonus_value = .02"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Viscount Pop Bonus', 'POP', 'BOTH', true, .03) ON DUPLICATE KEY UPDATE bonus_value = .03"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Count Pop Bonus', 'POP', 'BOTH', true, .05) ON DUPLICATE KEY UPDATE bonus_value = .05"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Marquis Pop Bonus', 'POP', 'BOTH', true, .07) ON DUPLICATE KEY UPDATE bonus_value = .07"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Duke Pop Bonus', 'POP', 'BOTH', true, .09) ON DUPLICATE KEY UPDATE bonus_value = .09"),
            new SimpleUpdateAction(
                    "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Prince Pop Bonus', 'POP', 'BOTH', true, .11) ON DUPLICATE KEY UPDATE bonus_value = .11"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Lord' AND bonus.name = 'Lord Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Baron' AND bonus.name = 'Baron Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Viscount' AND bonus.name = 'Viscount Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Count' AND bonus.name = 'Count Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Marquis' AND bonus.name = 'Marquis Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Duke' AND bonus.name = 'Duke Pop Bonus'"),
            new SimpleUpdateAction(
                    "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Prince' AND bonus.name = 'Prince Pop Bonus'")

        );
    }
}