package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV15ToV16 extends ApiMySQLDatabaseUpdater {

  @Override
  public int updatesToVersion() {
    return 16;
  }

  @Override
  public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
    return Lists.newArrayList(
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.4 where name = 'Halfling TPA'"),
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 where name = 'Orc Gains'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 1, elite_networth = 4.75 WHERE name = 'Avian'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 2, elite_networth = 5.25 WHERE name = 'Dwarf'"),
        new SimpleUpdateAction(
            "UPDATE race SET elite_off_strength = 3, elite_networth = 5.25, elite_sendout_percentage = 0 WHERE name = 'Halfling'"),
        new SimpleUpdateAction("UPDATE race SET elite_def_strength = 3, elite_networth = 6.5 WHERE name = 'Human'")
    );
  }
}
