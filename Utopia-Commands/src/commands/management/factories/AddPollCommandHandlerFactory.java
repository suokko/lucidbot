package commands.management.factories;

import api.commands.*;
import api.database.models.AccessLevel;
import com.google.inject.Provider;
import commands.CommandTypes;
import commands.management.handlers.AddPollCommandHandler;
import spi.commands.CommandHandler;
import spi.commands.CommandHandlerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Singleton
public class AddPollCommandHandlerFactory implements CommandHandlerFactory {
    private final Command handledCommand = CommandBuilder.forCommand("addpoll").ofType(CommandTypes.KD_MANAGEMENT).requiringAccessLevel(AccessLevel.ADMIN).withNonDowngradableAccessLevel().build();
    private final List<CommandParser> parsers = new ArrayList<>();

    private final Provider<AddPollCommandHandler> handlerProvider;

    @Inject
    public AddPollCommandHandlerFactory(final Provider<AddPollCommandHandler> handlerProvider) {
        this.handlerProvider = handlerProvider;

        handledCommand.setHelpText("Adds a poll, meaning you can start adding voting options to it. You then have to start it to let people vote. " +
                "Supports bindings to let you limit who is allowed to view and vote in the poll and so on");

        ParamParsingSpecification bindings = new ParamParsingSpecification("bindings", "\\{[^\\}]+\\}",
                CommandParamGroupingSpecification.OPTIONAL);
        ParamParsingSpecification descriptions = new ParamParsingSpecification("description", ".+");
        parsers.add(new CommandParser(bindings, descriptions));
    }

    @Override
    public Command getHandledCommand() {
        return handledCommand;
    }

    @Override
    public List<CommandParser> getParsers() {
        return Collections.unmodifiableList(parsers);
    }

    @Override
    public CommandHandler getCommandHandler() {
        return handlerProvider.get();
    }
}
