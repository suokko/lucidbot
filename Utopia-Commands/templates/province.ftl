<@ircmessage type="reply_message">
    <@compress single_line=true>
    >> ${OLIVE+province.name+NORMAL} << ${OLIVE+province.race.name} ${province.personality.name+NORMAL} played by ${RED+province.provinceOwner.mainNick+NORMAL}
    (<#if province.networth??>NW: ${stringUtil.colorWithAge(province.networth?string, province.lastUpdated)}<#else> NW: 0 </#if>
    <#if province.land??>Land: ${stringUtil.colorWithAge(province.land?string, province.lastUpdated)} <#else>Land: 0</#if>
    <#if province.modOffense??>ModOff: ${stringUtil.colorWithAge(province.modOffense?string, province.sot.lastUpdated)}<#else>ModOff: 0</#if>
    <#if province.modDefense??>ModDef: ${stringUtil.colorWithAge(province.modDefense?string, province.sot.lastUpdated)}<#else>ModDef: 0</#if>)
    </@compress>
</@ircmessage>
