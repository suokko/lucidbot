package api.filters;

import com.google.common.collect.Sets;

import org.testng.annotations.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

import spi.filters.AbstractFilter;
import spi.filters.Filter;
import spi.filters.FilterBuilder;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Test
public class FilterParserTest {

  public void testParsing_Combinable() {
    FilterParser cut = new FilterParser(Sets.<FilterBuilder>newHashSet(new TestFilterBuilder()));

    Collection<Filter<?>> filters = cut.parseFilters("[hello, goodbye]");
    Iterator<Filter<?>> iterator = filters.iterator();
    Filter<String> singleFilter = (Filter<String>) iterator.next();
    assertTrue(singleFilter.passesFilter("hello"));
    assertFalse(iterator.hasNext());
  }

  public void testParsing_NonCombinable() {
    FilterParser cut = new FilterParser(Sets.<FilterBuilder>newHashSet(new TestFilterBuilder()));

    Collection<Filter<?>> filters = cut.parseFilters("[!hello, !goodbye]");
    Iterator<Filter<?>> iterator = filters.iterator();
    Filter<String> firstFilter = (Filter<String>) iterator.next();
    assertFalse(firstFilter.passesFilter("hello"));
    Filter<String> secondFilter = (Filter<String>) iterator.next();
    assertTrue(secondFilter.passesFilter("hello"));

    assertFalse(firstFilter.isOrCombinable());
  }

  public void testParsing_MixedFilters() {
    FilterParser cut = new FilterParser(Sets.<FilterBuilder>newHashSet(new TestFilterBuilder()));

    Collection<Filter<?>> filters = cut.parseFilters("[!hello, !goodbye, awesome, cool]");
    Iterator<Filter<?>> iterator = filters.iterator();
    Filter<String> firstFilter = (Filter<String>) iterator.next();
    assertFalse(firstFilter.passesFilter("hello"));
    assertTrue(firstFilter.passesFilter("awesome"));
    Filter<String> secondFilter = (Filter<String>) iterator.next();
    assertFalse(secondFilter.passesFilter("goodbye"));
    assertTrue(secondFilter.passesFilter("awesome"));
    Filter<String> thirdFilter = (Filter<String>) iterator.next();
    assertFalse(thirdFilter.passesFilter("hello"));
    assertTrue(thirdFilter.passesFilter("awesome"));

    assertFalse(iterator.hasNext());
  }

  private static class TestFilter extends AbstractFilter<String> {

    private final String value;
    private final boolean isCombinable;

    protected TestFilter(final String value, final boolean isCombinable) {
      super(String.class);
      this.value = value;
      this.isCombinable = isCombinable;
    }

    @Override
    protected Class<? extends Filter<String>> getFilterType() {
      return getClass();
    }

    @Override
    public boolean isOrCombinable() {
      return isCombinable;
    }

    @Override
    public boolean passesFilter(final String value) {
      return isCombinable ? this.value.equals(value) : !this.value.equals(value);
    }
  }

  private static class TestFilterBuilder implements FilterBuilder {

    @Override
    public Filter<?> parseAndBuild(final String text) {
      boolean isCombinable = !text.startsWith("!");
      return new TestFilter(isCombinable ? text : text.substring(1), isCombinable);
    }

    @Override
    public Pattern getFilterPattern() {
      return Pattern.compile(".*");
    }
  }

}