package database.models;

import api.database.models.BotUser;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "poll_vote")
@NoArgsConstructor
@EqualsAndHashCode(of = {"pollOption", "user"})
@Getter
@Setter
public class PollVote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "poll_option_id", nullable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PollOption pollOption;

    @ManyToOne(optional = false)
    @JoinColumn(name = "bot_user_id", nullable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private BotUser user;

    public PollVote(final PollOption pollOption, final BotUser user) {
        this.pollOption = pollOption;
        this.user = user;
    }
}
