/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.team.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import api.tools.files.FilterUtil;
import database.daos.PollDAO;
import database.daos.UserActivitiesDAO;
import database.models.Poll;
import database.models.UserActivities;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.BindingsManager;
import tools.RecentActivitiesFinder;
import tools.user_activities.RecentActivityType;
import tools.user_activities.UnseenInfo;

import javax.inject.Inject;
import java.util.*;

import static api.tools.text.StringUtil.nullToEmpty;
import static api.tools.text.StringUtil.splitOnSpace;
import static api.tools.time.DateUtil.isBefore;

public class PollsCommandHandler implements CommandHandler {

    private final PollDAO pollDAO;
    private final BindingsManager bindingsManager;
    private final UserActivitiesDAO userActivitiesDAO;
    private final RecentActivitiesFinder recentActivitiesFinder;

    @Inject
    public PollsCommandHandler(final PollDAO pollDAO,
                               final BindingsManager bindingsManager,
                               final UserActivitiesDAO userActivitiesDAO,
                               final RecentActivitiesFinder recentActivitiesFinder) {
        this.pollDAO = pollDAO;
        this.bindingsManager = bindingsManager;
        this.userActivitiesDAO = userActivitiesDAO;
        this.recentActivitiesFinder = recentActivitiesFinder;
    }

    @Override
    public CommandResponse handleCommand(final IRCContext context,
                                         final Params params,
                                         final Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            final BotUser user = context.getBotUser();
            UserActivities userActivities = userActivitiesDAO.getUserActivities(user);

            List<Poll> polls = new LinkedList<>();
            Set<String> types = new HashSet<>(Arrays.asList(splitOnSpace(nullToEmpty(params.getParameter("type")))));
            if (user.isAdmin() && types.contains("*")) {
                polls.addAll(pollDAO.getAllPolls());
            } else {
                polls.addAll(pollDAO.getPollsForUser(user, bindingsManager));
            }

            if (types.contains("new")) {
                removeOld(polls, userActivities.getLastPollsCheck());
            }
            if (!types.contains("closed")) {
                removeClosed(polls);
            }

            FilterUtil.applyFilters(polls, filters);

            if (polls.isEmpty()) return CommandResponse.errorResponse("No matches found");

            userActivities.setLastPollsCheck(new Date());
            List<UnseenInfo> unseenInfoOfInterest = recentActivitiesFinder.mapUnseenActivities(user, userActivities);
            return CommandResponse.resultResponse("polls", polls, "unseenInfoOfInterest", unseenInfoOfInterest, "thisActivityType",
                    RecentActivityType.POLLS);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }

    private static void removeOld(final Collection<Poll> polls, final Date lastPollsCheck) {
        Iterator<Poll> iterator = polls.iterator();
        while (iterator.hasNext()) {
            Poll next = iterator.next();
            if (isBefore(next.getAdded(), lastPollsCheck)) iterator.remove();
        }
    }

    private static void removeClosed(final Collection<Poll> polls) {
        Iterator<Poll> iterator = polls.iterator();
        while (iterator.hasNext()) {
            Poll next = iterator.next();
            if (next.isClosed()) iterator.remove();
        }
    }
}
