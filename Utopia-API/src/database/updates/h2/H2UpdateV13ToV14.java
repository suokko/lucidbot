package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV13ToV14 extends ApiH2DatabaseUpdater {
    @Override
    public int updatesToVersion() {
      return 14;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE forum_post ALTER COLUMN bot_user_id DROP NOT NULL")
        );
    }
}
