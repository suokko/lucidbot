package events;

import com.google.common.collect.Multimap;
import database.models.NewsItem;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class IncomingAttacksEvent {
    private final Multimap<String, NewsItem> incomingAttacksPerProvince;
}
