package database.updates.h2;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import lombok.extern.log4j.Log4j;

@Log4j
public class H2UpdateV25ToV26 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 26;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(new DatabaseUpdateAction() {
                    @Override
                    public void runDatabaseAction(final Connection connection) throws SQLException {
                        PreparedStatement statement = connection.prepareStatement(
                                "SELECT DISTINCT constraint_name FROM information_schema.constraints WHERE table_name='INSTANT_OP' and column_list='BOT_USER_ID,PROVINCE_ID,OP_TYPE_ID'");
                        ResultSet resultSet = statement.executeQuery();
                        if (resultSet.next()) {
                            String constraintName = resultSet.getString("constraint_name");

                            CleanupUtil.closeSilently(resultSet);
                            CleanupUtil.closeSilently(statement);

                            statement = connection.prepareStatement("ALTER TABLE INSTANT_OP DROP CONSTRAINT " + constraintName);
                            statement.executeUpdate();
                            CleanupUtil.closeSilently(statement);
                        }
                    }
                },
                new SimpleUpdateAction("ALTER TABLE instant_op ADD COLUMN damage_type VARCHAR(100)"),
                new SimpleUpdateAction("UPDATE instant_op SET damage_type = 'NONE'"),
                new SimpleUpdateAction("ALTER TABLE instant_op ALTER COLUMN damage_type VARCHAR(100) NOT NULL"),
                new SimpleUpdateAction("UPDATE op_type SET op_character = 'INSTANT_SPELLOP_WITHOUT_PROVINCE' WHERE name = 'Propaganda'"),
                new SimpleUpdateAction("UPDATE op_type SET op_regex = 'We have converted (?<result>[\\\\d,]+) (of the enemy''s )?(?<damagetype>[\\\\w]+) (troops to our army|from the enemy)' WHERE name = 'Propaganda'"),
                new SimpleUpdateAction("UPDATE op_type SET short_name = 'prop' WHERE name = 'Propaganda'")
        );
    }
}