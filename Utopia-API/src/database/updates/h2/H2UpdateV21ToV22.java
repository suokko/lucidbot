package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV21ToV22 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 22;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Halfling TPA', 'TPA', 'BOTH', true, 0.5)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Elf WPA', 'WPA', 'BOTH', true, 0.4)"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling TPA'"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Elf' AND bonus.name = 'Elf WPA'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND bonus.type = 'TPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND bonus.type = 'WPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery TPA'"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery WPA'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, elite_networth = 5.25 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 4, elite_networth = 5.5 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.5 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.25 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 5.75 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.0 WHERE name = 'Orc'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Warrior OME In War', 'OME_IN_WAR', 'BOTH', true, .2)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Warrior' AND bonus.name = 'Warrior OME In War'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Human' AND bonus.type = 'OME_IN_WAR' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human OME In War'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.race_id = bonus.id WHERE " +
                        "race.name = 'Halfling' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Halfling Gains'")
        );
    }
}