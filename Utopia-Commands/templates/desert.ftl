<@ircmessage type="reply_message">
    <@compress single_line=true>
    Desertion for >> ${OLIVE+province+NORMAL} <<:
    ${stringUtil.colorWithAge(result.elites?string, lastUpdated)} Elites,
    ${stringUtil.colorWithAge(result.thieves?string, lastUpdated)} Thieves,
    ${stringUtil.colorWithAge(result.defspecs?string, lastUpdated)} DefSpecs,
    ${stringUtil.colorWithAge(result.offspecs?string, lastUpdated)} OffSpecs,
    ${stringUtil.colorWithAge(result.soldiers?string, lastUpdated)} Soldiers.
    Soldiers needed to cover trained unit desertions: ${RED+result.cover+NORMAL}.
    </@compress>
</@ircmessage>