<#if sos.province.provinceOwner??>
<#else>
${OLIVE}A ${RED}SoS${OLIVE} was just added for ${BLUE+sos.province.name} ${sos.province.kingdom.location+OLIVE} by ${NORMAL}o${RED+sos.savedBy+NORMAL}o
</#if>