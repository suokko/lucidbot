package database.daos;

import api.database.AbstractDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import com.google.inject.Provider;
import database.models.Poll;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tools.BindingsManager;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static api.tools.time.DateUtil.isAfter;

@ParametersAreNonnullByDefault
public class PollDAO extends AbstractDAO<Poll> {

    @Inject
    public PollDAO(final Provider<Session> sessionProvider) {
        super(Poll.class, sessionProvider);
    }

    @Transactional
    @Nullable
    public Poll getPoll(final long id) {
        return get(Restrictions.idEq(id));
    }

    @Transactional
    public Collection<Poll> getAllPolls() {
        return find(Order.asc("id"));
    }

    @Transactional
    public List<Poll> getPollsForUser(final BotUser botUser, final BindingsManager bindingsManager) {
        List<Poll> out = new ArrayList<>();
        for (Poll poll : getAllPolls()) {
            if (bindingsManager.matchesBindings(poll.getBindings(), botUser)) out.add(poll);
        }
        return out;
    }

    @Transactional
    public List<Poll> getNewPollsForUser(final Date lastCheck, final BotUser botUser, final BindingsManager bindingsManager) {
        List<Poll> out = new ArrayList<>();
        for (Poll poll : getPollsForUser(botUser, bindingsManager)) {
            if (isAfter(poll.getAdded(), lastCheck)) out.add(poll);
        }
        return out;
    }

    @Transactional
    public int countPollsForUserAddedAfter(final Date date, final BotUser botUser, final BindingsManager bindingsManager) {
        return getNewPollsForUser(date, botUser, bindingsManager).size();
    }

}
