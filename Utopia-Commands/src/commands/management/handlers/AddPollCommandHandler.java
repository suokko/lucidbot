package commands.management.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.PollDAO;
import database.models.Bindings;
import database.models.Poll;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.BindingsManager;

import javax.inject.Inject;
import java.util.Collection;

public class AddPollCommandHandler implements CommandHandler {

    private final PollDAO pollDAO;
    private final BindingsManager bindingsManager;

    @Inject
    public AddPollCommandHandler(final PollDAO pollDAO, final BindingsManager bindingsManager) {
        this.pollDAO = pollDAO;
        this.bindingsManager = bindingsManager;
    }

    @Override
    public CommandResponse handleCommand(final IRCContext context,
                                         final Params params,
                                         final Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Bindings bindings = bindingsManager.parseBindings(params.getParameter("bindings"));
            String description = params.getParameter("description");
            Poll poll = new Poll(description, bindings, context.getUser().getMainNick());
            poll = pollDAO.save(poll);
            return CommandResponse.resultResponse("poll", poll);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }

}
