package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV19ToV20 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 20;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Human OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .15) ON DUPLICATE KEY UPDATE bonus_value = .15"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Human' AND bonus.name = 'Human OME In War'"),
                new SimpleUpdateAction("ALTER TABLE sot ADD COLUMN war BIT NOT NULL")
        );
    }
}