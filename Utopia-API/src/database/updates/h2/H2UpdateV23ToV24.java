package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV23ToV24 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 24;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN max_population INT"),
                new SimpleUpdateAction("UPDATE province SET max_population = 0"),
                new SimpleUpdateAction("ALTER TABLE province MODIFY COLUMN max_population INT NOT NULL")
        );
    }
}