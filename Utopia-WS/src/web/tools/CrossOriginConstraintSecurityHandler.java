

package web.tools;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.RoleInfo;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;

public class CrossOriginConstraintSecurityHandler extends ConstraintSecurityHandler {
    @Override
    protected boolean isAuthMandatory(Request baseRequest, Response baseResponse, Object constraintInfo) {
        if (constraintInfo == null) {
            return false;
        }
        if (isPreFlightRequest(baseRequest)) {
            return false;
        }
        return ((RoleInfo) constraintInfo).isChecked();
    }
    private static boolean isPreFlightRequest(Request request) {
        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            return true;
        }
        return false;
    }
}