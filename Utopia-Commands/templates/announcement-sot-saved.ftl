<#if sot.province.provinceOwner??>
<#else>
${OLIVE}A ${RED}SoT${OLIVE} was just added for ${BLUE+sot.province.name} ${sot.province.kingdom.location+OLIVE} by ${NORMAL}o${RED+sot.savedBy+NORMAL}o
</#if>