package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;

public class H2UpdateV10ToV11 extends ApiH2DatabaseUpdater {
    @Override
    public int updatesToVersion() {
        return 11;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE user_activities ADD last_polls_check TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL")
        );
    }
}
