<@ircmessage type="reply_notice">
    <#include "news_bar.ftl">

Polls:
    <#list polls as poll>
    [id:${poll.id} - <#if poll.closed>${RED}closed${NORMAL}<#else>${GREEN}open${NORMAL}</#if>]: <#if poll.closed>${DARK_GRAY}<#else>${RED}</#if>${poll.description+NORMAL}
    Added ${DARK_GRAY+timeUtil.compareDateToCurrent(poll.added)+NORMAL} ago by ${DARK_GRAY+poll.addedBy+NORMAL}
    </#list>
</@ircmessage>