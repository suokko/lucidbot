
package listeners;

import lombok.extern.log4j.Log4j;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.DeadEvent;
import spi.events.EventListener;


/**
 * Logs on dead events (events without a listener)
 */
@Log4j
class DeadEventListener implements EventListener {

    @Subscribe
    public void onDeadEvent(final DeadEvent event) {
        DeadEventListener.log.info("Dead Event found: " + event.getEvent());
    }
}