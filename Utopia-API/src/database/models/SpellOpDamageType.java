package database.models;

import api.common.HasName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kyle on 11/8/2015.
 */
public enum SpellOpDamageType implements HasName{
    SPECS("specialist"),
    ELITES("elites"),
    THIEVES("thieves"),
    WIZARDS("wizards"),
    SOLDIERS("soldiers"),
    NONE("none");

    private final String name;

    SpellOpDamageType(final String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public static SpellOpDamageType getByName(String name) {
        for (SpellOpDamageType spellOpDamageType : values()) {
            if (spellOpDamageType.name.equalsIgnoreCase(name)) return spellOpDamageType;
        }
        Pattern elitesPattern = Pattern.compile("Elf Lords|Berserkers|Brutes|Ghouls|Ogres|Beastmasters|Drakes|Knights");
        Matcher matcher = elitesPattern.matcher(name);
        if (matcher.find()) {
            return ELITES;
        }
        return null;
    }
}
