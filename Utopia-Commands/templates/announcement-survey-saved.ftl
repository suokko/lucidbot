<#if survey.province.provinceOwner??>
<#else>
${OLIVE}A ${RED}Survey${OLIVE} was just added for ${BLUE+survey.province.name} ${survey.province.kingdom.location+OLIVE} by ${NORMAL}o${RED+survey.savedBy+NORMAL}o
</#if>