package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV18ToV19 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 19;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Double TDs', 'BUILDING_EFFECT', 'BOTH', true, 1.0)"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Rogue' AND bonus.name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "UPDATE science_type SET angel_name = 'Gains in Combat' WHERE name = 'Military'")
        );
    }
}