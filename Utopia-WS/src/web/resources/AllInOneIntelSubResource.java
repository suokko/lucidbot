package web.resources;

import com.google.common.base.Function;
import com.google.inject.Provider;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import database.daos.IntelDAO;
import database.daos.ProvinceDAO;
import database.models.Kingdom;
import database.models.Province;
import database.models.SoM;
import database.models.SoS;
import database.models.SoT;
import database.models.Survey;
import intel.Intel;
import intel.IntelParser;
import intel.IntelParserManager;
import lombok.extern.log4j.Log4j;
import tools.parsing.UtopiaValidationType;
import web.models.RS_Kingdom;
import web.models.RS_Province;
import web.models.RS_SoM;
import web.models.RS_SoS;
import web.models.RS_SoT;
import web.models.RS_Survey;
import web.tools.WebContext;

@Log4j
public class AllInOneIntelSubResource {

  private static final Pattern
      INFILTRATE = Pattern.compile("Our thieves have infiltrated the Thieves' Guilds of (?<target>[^(]+" +
                                   UtopiaValidationType.KDLOC.getPatternString() +
                                   "). They appear to have about (?<result>" + ValidationType.INT.getPattern() +
                                   ") thieves employed across their lands");
    private static JAXBContext JAXB_CONTEXT;
    private static final Map<Class<? extends Intel>, Function<Intel, Object>> converters = new HashMap<>();

    static {
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(RS_Kingdom.class, RS_SoM.class, RS_SoS.class, RS_SoT.class, RS_Survey.class, ParsedIntel.class);
        } catch (JAXBException e) {
            AllInOneIntelSubResource.log.warn("Failed to create jaxb context. Posting to the all in one formatter will fail to produce a proper response", e);
        }

        converters.put(Kingdom.class, new Function<Intel, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable final Intel intel) {
                return RS_Kingdom.fromKingdom((Kingdom) intel, RS_Province.MINIMAL_PROVINCE_CONVERTER, false);
            }
        });
        converters.put(SoM.class, new Function<Intel, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable final Intel intel) {
                return RS_SoM.fromSoM((SoM) intel, false);
            }
        });
        converters.put(SoS.class, new Function<Intel, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable final Intel intel) {
                return RS_SoS.fromSoS((SoS) intel, false);
            }
        });
        converters.put(SoT.class, new Function<Intel, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable final Intel intel) {
                return RS_SoT.fromSoT((SoT) intel, false);
            }
        });
        converters.put(Survey.class, new Function<Intel, Object>() {
            @Nullable
            @Override
            public Object apply(@Nullable final Intel intel) {
                return RS_Survey.fromSurvey((Survey) intel, false);
            }
        });
    }

    private final IntelDAO intelDAO;
    private final ProvinceDAO provinceDAO;
    private final Provider<IntelParserManager> intelParserManagerProvider;
    private final Provider<DelayedEventPoster> delayedEventPosterProvider;

    @Inject
    public AllInOneIntelSubResource(final IntelDAO intelDAO,
                                    final ProvinceDAO provinceDAO,
                                    final Provider<IntelParserManager> intelParserManagerProvider,
                                    final Provider<DelayedEventPoster> delayedEventPosterProvider) {
        this.intelDAO = intelDAO;
        this.provinceDAO = provinceDAO;
        this.intelParserManagerProvider = intelParserManagerProvider;
        this.delayedEventPosterProvider = delayedEventPosterProvider;
    }

    String addIntel(final String newIntel, final WebContext webContext) throws Exception {
        String cleanedIntel = newIntel.replace('\r', ' ').replace('\n', ' ').trim();
        Map<String, IntelParser<?>> parsers = intelParserManagerProvider.get().getParsers(cleanedIntel);

      boolean foundAtLeastOneInfil = parseInfiltrates(cleanedIntel);

      if (parsers.isEmpty() && !foundAtLeastOneInfil) {
        throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                                              .entity("No parsable intel found").build());
        }

        BotUser botUser = webContext.getBotUser();

        List<Object> parsedObjects = new ArrayList<>();
        StringWriter writer = new StringWriter();
        for (Map.Entry<String, IntelParser<?>> entry : parsers.entrySet()) {
            String rawIntel = entry.getKey();
            IntelParser<?> parser = entry.getValue();

            try {
                Intel parsedIntel = parser.parse(botUser.getMainNick(), rawIntel);
                if (parsedIntel != null) {
                    DelayedEventPoster delayedEventPoster = delayedEventPosterProvider.get();
                    intelDAO.saveIntel(parsedIntel, botUser.getId(), delayedEventPoster);
                    delayedEventPoster.execute();
                    Function<Intel, Object> converter = converters.get(parsedIntel.getIntelType());
                    parsedObjects.add(converter.apply(parsedIntel));
                }
            } catch (Exception e) {
                AllInOneIntelSubResource.log.error("Failed to parse or save intel posted from web service", e);
              throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
            }
        }

        JAXB_CONTEXT.createMarshaller().marshal(new ParsedIntel(parsedObjects), writer);
        return writer.toString();
    }

  private boolean parseInfiltrates(final String cleanedIntel) {
    Matcher matcher = INFILTRATE.matcher(cleanedIntel);
    boolean foundAtLeastOneInfil = false;
    while (matcher.find()) {
      foundAtLeastOneInfil = true;
      String target = matcher.group("target");
      final String provinceName = target.substring(0, target.indexOf('(')).trim();
      final String kingdom = target.substring(target.indexOf('('));
      final int result = NumberUtil.parseInt(matcher.group("result"));

      try {
        saveInfiltrate(provinceName, kingdom, result);
      } catch (final Exception e) {
        throw new WebApplicationException(Response.serverError().entity("Failed to update tpa").build());
      }
    }
    return foundAtLeastOneInfil;
  }

  private void saveInfiltrate(final String provinceName, final String kingdom, final int result) {
    Province province = provinceDAO.getOrCreateProvince(provinceName, kingdom);
    province.setThieves(result);
    province.setThievesLastUpdated(new Date());
    province.setLastUpdated(province.getThievesLastUpdated(), true);
    }

    @XmlRootElement(name = "ParsedIntel")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class ParsedIntel {
        private List<Object> intel;

        public ParsedIntel() {
        }

        public ParsedIntel(final List<Object> intel) {
            this.intel = intel;
        }
    }

}
